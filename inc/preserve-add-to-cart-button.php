<?php

/**
 * Inject custom add-to-cart.js
 */
add_filter( 'script_loader_src', function( $src, $handle ) {
	if ( 'wc-add-to-cart' === $handle ) {
		$src = starter()->plugin_dir_url . 'public/js/add-to-cart.js';
	}
	return $src;
}, 10, 2 );
