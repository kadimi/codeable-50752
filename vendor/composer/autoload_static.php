<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit856e1def98938701ad438b05b553c4f6
{
    public static $files = array (
        'a5f882d89ab791a139cd2d37e50cdd80' => __DIR__ . '/..' . '/tgmpa/tgm-plugin-activation/class-tgm-plugin-activation.php',
        '6bc45d0537e6858fd179bdbc31d62c79' => __DIR__ . '/..' . '/raveren/kint/Kint.class.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {

        }, null, ClassLoader::class);
    }
}
